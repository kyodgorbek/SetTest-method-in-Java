# SetTest-method-in-Java



import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.JOptionPane;

public class SetTest 
{ 
   public static void main(String[] args){
     
      Set names = new HashSet();
      
      boolean done = false;
      while (!done)
      {
         String input = JOptionPane.showInputDialog("Add name, Cancel when done");
         if (input == null)
          done = true;
        else
        {
            names.add(input);
            print(names);
         }
        }
        
        done = false;
        while (!done)
        {
           String input = JOptionPane.showInputDialog("Remove name, Cancel when done");
           if (input == null)
             done = true;
           else
           {
              names.remove(input);
              print(names);
           }
         }
         System.exit(0);
      }
      
      private static void print(Set s)
      {
        Iterator iter = s.iterator();
        System.out.print("{");
        while (iter.hasNext())
        {
           System.out.print(iter.next());
           System.out.println(" ");
        }          
        
        System.out.println("}");
      }
    }                
